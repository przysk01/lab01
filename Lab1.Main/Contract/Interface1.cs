﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface ISsak
    {
        string ssak();
    }

    public interface IPies : ISsak
    {
        string hau();
    }

    public interface IKot : ISsak
    {
        string miau();
    }

    public interface IRobot : IKot, ISsak
    {
        string Laser();
        string ssak();
        string Plazma();
    }
}
