﻿using System;
using Lab1.Contract;
using Lab1.Implementation;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Main
{
    public class Program
    {
        public void collectionConsumerMethod(List<ISsak> list)
        {
            foreach (var a in list) a.ssak();
        }

        public IList<ISsak> collectionFactoryMethod()
        {
            IList<ISsak> lista = new List<ISsak>() { new Kot(), new Pies() };
            return lista;
        }
        
        static void Main(string[] args)
        {
            List<ISsak> Zwierzeta = new List<ISsak>();
          
            Zwierzeta.Add(new Kot());
            Zwierzeta.Add(new Pies());
            Zwierzeta.Add(new cyborg());

            foreach (var i in Zwierzeta)
            {
                Console.WriteLine(i.ssak());
            }
            Console.ReadKey();

        }
    }
}
